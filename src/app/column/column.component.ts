import { Component, OnInit, Input } from '@angular/core';
import { DragulaService } from 'ng2-dragula';

@Component({
  selector: 'app-column',
  templateUrl: './column.component.html',
  styleUrls: ['./column.component.scss']
})
export class ColumnComponent implements OnInit {

  @Input() list: object;
  constructor(private dragula: DragulaService) {
   }

  ngOnInit() {
    this.dragula
      .drag()
      .subscribe(value => {
        console.log(value);
      });

    this.dragula
    .drop()
    .subscribe(value => {
        console.log(value);
      });
  }

}
