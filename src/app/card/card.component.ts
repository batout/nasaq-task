import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() card: object;
  constructor() {
   }

  ngOnInit() {
  }

  playVid(event: Event) {
    console.log(event);
    event['toElement']['style']['visibility'] = 'hidden';
    event['toElement']['nextElementSibling'].play();
    event['toElement']['nextElementSibling'].onended = function() {
      event['toElement']['style']['visibility'] = 'visible';
  };
    console.log(document.getElementById('play-btn').nextSibling);
  }

  getLeftPosition(i) {
    return 20 + i * 30 - i * (10 + i * 2 ) + 'px';
  }

}
