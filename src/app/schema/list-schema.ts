export class ListSchema {
  name: String;
  color: String;
  cards: Array<object>;
}
