import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// import the app components
import { NavbarComponent } from './navbar/navbar.component';
import { BoardSelectorComponent } from './board-selector/board-selector.component';
import { ColumnComponent } from './column/column.component';
import { CardComponent } from './card/card.component';
import { BoardComponent } from './board/board.component';

// custom modules
import { DragulaModule } from 'ng2-dragula';
import { FabButtonComponent } from './fab-button/fab-button.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    BoardSelectorComponent,
    ColumnComponent,
    CardComponent,
    BoardComponent,
    FabButtonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DragulaModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
