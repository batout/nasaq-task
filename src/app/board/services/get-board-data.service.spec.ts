import { TestBed } from '@angular/core/testing';

import { GetBoardDataService } from './get-board-data.service';

describe('GetBoardDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetBoardDataService = TestBed.get(GetBoardDataService);
    expect(service).toBeTruthy();
  });
});
