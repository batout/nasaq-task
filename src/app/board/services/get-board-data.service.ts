import { Injectable } from '@angular/core';
import { ListSchema } from '../../schema/list-schema';

@Injectable({
  providedIn: 'root'
})
export class GetBoardDataService {
  lists: ListSchema[] = [
    {
      name: 'Design area',
      color: '#7148fc',
      cards: [
        {
          taskName: 'Task one',
          attachmentCount: 0,
          description: `Lorem Ipsum is simply dummy text
                        of the printing and typesetting
                        industry. Lorem Ipsum has been
                        the industry's standard dummy text
                        ever since the 1500s`,
          imageUrl: '../../assets/images/coffe.jpg',
          videoUrl: '../../assets/videos/SampleVideo.mp4',
          followers: [
            '../../assets/images/avatar1.png',
            '../../assets/images/avatar2.png',
            '../../assets/images/avatar3.png',
            '../../assets/images/avatar.png',
            '../../assets/images/avatar2.png',
            '../../assets/images/avatar3.png'
          ],
          isHalfCompleted: true
        },
        {
          taskName: 'Task two',
          attachmentCount: 3,
          description: null,
          imageUrl: null,
          videoUrl: null,
          followers: [
            '../../assets/images/avatar1.png',
            '../../assets/images/avatar2.png',
            '../../assets/images/avatar3.png',
            '../../assets/images/avatar.png',
            '../../assets/images/avatar2.png',
            '../../assets/images/avatar3.png'
          ],
          isHalfCompleted: true
        }
      ]
    },
    {
      name: 'Frontend area',
      color: '#f5a623',
      cards: [
        {
          taskName: 'Task two',
          attachmentCount: 3,
          description: null,
          imageUrl: null,
          videoUrl: null,
          followers: [
            '../../assets/images/avatar1.png',
            '../../assets/images/avatar2.png',
            '../../assets/images/avatar3.png',
            '../../assets/images/avatar.png',
            '../../assets/images/avatar2.png',
            '../../assets/images/avatar3.png'
          ],
          isHalfCompleted: true
        },
        {
          taskName: 'Task two',
          attachmentCount: 0,
          description: `Lorem Ipsum is simply dummy text
                        of the printing and typesetting
                        industry. Lorem Ipsum has been
                        the industry's standard dummy text
                        ever since the 1500s`,
          imageUrl: '../../assets/images/coffe.jpg',
          videoUrl: '../../assets/videos/SampleVideo.mp4',
          followers: [
            '../../assets/images/avatar1.png',
            '../../assets/images/avatar2.png',
            '../../assets/images/avatar3.png',
            '../../assets/images/avatar.png',
            '../../assets/images/avatar2.png',
            '../../assets/images/avatar3.png'
          ],
          isHalfCompleted: null,
          isCompleted: true
        },
        {
          taskName: 'Task three',
          attachmentCount: 3,
          description: null,
          imageUrl: '../../assets/images/coffe.jpg',
          videoUrl: null,
          followers: [
          ],
          isHalfCompleted: null,
          isCompleted: null
        }
      ]
    },
    {
      name: 'Development area',
      color: '#4a90e2',
      cards: [
        {
          taskName: 'Task three',
          attachmentCount: 3,
          description: null,
          imageUrl: '../../assets/images/coffe.jpg',
          videoUrl: null,
          followers: [
          ],
          isHalfCompleted: null,
          isCompleted: null
        },
        {
          taskName: 'Task one',
          attachmentCount: 0,
          description: `Lorem Ipsum is simply dummy text
                        of the printing and typesetting
                        industry. Lorem Ipsum has been
                        the industry's standard dummy text
                        ever since the 1500s`,
          imageUrl: '../../assets/images/coffe.jpg',
          videoUrl: '../../assets/videos/SampleVideo.mp4',
          followers: [
            '../../assets/images/avatar1.png',
            '../../assets/images/avatar2.png',
            '../../assets/images/avatar3.png',
            '../../assets/images/avatar.png',
            '../../assets/images/avatar2.png',
            '../../assets/images/avatar3.png'
          ],
          isHalfCompleted: true
        }
      ]
    },
    {
      name: 'Board Name',
      color: '#04cda0',
      cards: [
        {
          taskName: 'Task one',
          attachmentCount: 2,
          description: null,
          imageUrl: null,
          videoUrl: null,
          followers: [
            '../../assets/images/avatar1.png',
            '../../assets/images/avatar2.png',
            '../../assets/images/avatar3.png',
            '../../assets/images/avatar.png',
            '../../assets/images/avatar2.png',
            '../../assets/images/avatar3.png'
          ],
          isHalfCompleted: true
        },
        {
          taskName: 'Task two',
          attachmentCount: 0,
          description: `Lorem Ipsum is simply dummy text
                        of the printing and typesetting
                        industry. Lorem Ipsum has been
                        the industry's standard dummy text
                        ever since the 1500s`,
          imageUrl: '../../assets/images/coffe.jpg',
          videoUrl: '../../assets/videos/SampleVideo.mp4',
          followers: [
            '../../assets/images/avatar1.png',
            '../../assets/images/avatar2.png',
            '../../assets/images/avatar3.png',
            '../../assets/images/avatar.png',
            '../../assets/images/avatar2.png',
            '../../assets/images/avatar3.png'
          ],
          isHalfCompleted: null,
          isCompleted: true
        },
        {
          taskName: 'Task three',
          attachmentCount: 3,
          description: null,
          imageUrl: '../../assets/images/coffe.jpg',
          videoUrl: null,
          followers: [
          ],
          isHalfCompleted: null,
          isCompleted: null
        }
      ]
    }
  ];
  constructor() { }

  getData() {
    return this.lists ;
  }
}
