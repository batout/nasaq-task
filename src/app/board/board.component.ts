import { GetBoardDataService } from './services/get-board-data.service';
import { Component, OnInit } from '@angular/core';
import { ListSchema } from '../schema/list-schema';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {
  lists = [];

  constructor(private _GetBoardDataService: GetBoardDataService) {
    this.lists = this._GetBoardDataService.getData();
   }

  ngOnInit() {

  }

}
